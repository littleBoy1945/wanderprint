# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
# from itemadapter import ItemAdapter
# import codecs
# import json
import requests

class DownloadImagePipeline(object):
    def process_item(self, item, spider):
        url = item.get('url')
        image_name = url.split('/')[-1]
        output = item['output']

        with open(output + image_name, 'wb') as f:
            f.write(requests.get(url).content)
        return item