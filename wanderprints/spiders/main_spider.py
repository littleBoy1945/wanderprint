import scrapy
from scrapy import Request
from scrapy.shell import inspect_response
import json
import time
from sys import platform
import requests

class MainSpiderSpider(scrapy.Spider):
    name = 'main_spider'

    def __init__(self, **kwargs):
        super(MainSpiderSpider, self).__init__(**kwargs)
        self.allowed_domains = ['sh.customily.com', 'app.customily.com']
        self.start_urls = [self.link]
        if self.output[-1] != '/' or self.output != '\\' :
            left_sigs = self.output.split('/')
            right_sigs = self.output.split('\\')
            if len(left_sigs) > len(right_sigs):
                self.output = self.output + '/'
            else:
                self.output = self.output + '\\'

    def start_requests(self):
        for i in self.start_urls:
            yield Request(url=i, callback = self.parse_product_info)

    def parse_product_info(self, response):
        json_response = json.loads(response.text)
        inital_product_id = json_response["productConfig"]["initial_product_id"]
        library_url = "https://app.customily.com/api/Product/GetProduct?productId={}&clientVersion=3.7.13".format(inital_product_id)
        yield Request(url=library_url, callback = self.get_library)

    def get_library(self, response):
        res = json.loads(response.text)
        res_previews = res["preview"]["imagePlaceHoldersPreview"]
        image_previews = list(map(lambda image_preview : image_preview["imageLibraryId"], res_previews))
        image_preview_ids = list(filter(None, image_previews))
        
        had_value_ids = []
        for img_id in image_preview_ids:
            build_url = "https://app.customily.com/api/Libraries/{}/Elements/Position/{}".format(img_id, 1)
            request = requests.get(build_url)
            time.sleep(2)
            if request.json() != None and img_id not in had_value_ids:
                had_value_ids.append(img_id)

        print("------------had_value_id----------.{}".format(had_value_ids))
        for img_id in had_value_ids:
            for i in range(1, 300):
                time.sleep(0.5)
                build_url = "https://app.customily.com/api/Libraries/{}/Elements/Position/{}".format(img_id, i)
                request = requests.get(build_url)
                time.sleep(3)
                res = request.json()
                if res:
                    tail_url = res["Path"].split("product-images")[1]
                    url = "https://cdn.customily.com/product-images{}".format(tail_url)
                    image_name = url.split('/')[-1]
                    output = self.output
                    with open(output + image_name, 'wb') as f:
                        f.write(requests.get(url).content)
                        f.close()
                else:
                    break
        
    # def get_mockup(self, response):
    #     res = json.loads(response.text)
    #     tail_url = res["Path"].split("product-images")[1]

    #     url = "https://cdn.customily.com/product-images{}".format(tail_url)
    #     image_name = url.split('/')[-1]
    #     output = self.output

    #     with open(output + image_name, 'wb') as f:
    #         f.write(requests.get(url).content)

    #     yield {
    #         "url": url,
    #         "output": output,
    #         "platform": platform
    #     }