import crochet
crochet.setup()

from flask import Flask , render_template, jsonify, request, redirect, url_for
from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
import time

from wanderprints.spiders.main_spider import MainSpiderSpider
from scrapy.utils.log import configure_logging

app = Flask(__name__)

output_data = []
configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
crawl_runner = CrawlerRunner()

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/', methods=['POST'])
def submit():
    if request.method == 'POST':
        s = request.form['url'] 
        l =  request.form['output']
        global baseURL
        global outPut
        baseURL = s
        outPut = l

        return redirect(url_for('scrape'))


@app.route("/scrape")
def scrape():

    scrape_with_crochet(url=baseURL, output=outPut)

    time.sleep(600)
    
    return jsonify(output_data)


@crochet.run_in_reactor
def scrape_with_crochet(url, kind='', output=''):
    dispatcher.connect(_crawler_result, signal=signals.item_scraped)
    eventual = crawl_runner.crawl(MainSpiderSpider, link = url, output = output)
    return eventual

#This will append the data to the output data list.
def _crawler_result(item, response, spider):
    output_data.append(dict(item))


if __name__== "__main__":
    app.run(debug=True)