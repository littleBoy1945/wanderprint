from PyQt5.QtWidgets import * 
from PyQt5.QtGui import * 
from PyQt5.QtCore import * 
import sys
import time
import subprocess
from os.path import expanduser

class Window(QDialog):  
    def __init__(self):
        super(Window, self).__init__()
  
        # setting window title
        self.setWindowTitle("Tool crawl images by NguyenTA ^_^")
  
        # setting geometry to the window
        self.setGeometry(100, 100, 600, 800)
  
        # creating a group box
        self.formGroupBox = QGroupBox("Form")
  
        # creating spin box to select output
        # self.output = QLineEdit()
        self.output = QLineEdit(placeholderText='Select path...')
        self.folder_picker = QToolButton(text='...')
        # creating combo box to select degree
        self.pages = QComboBox()
  
        # adding items to the combo box
        self.pages.addItems(["wanderprint", "89customized"])
  
        # creating a line edit
        self.link = QLineEdit()
  
        # creating a dialog button for ok and cancel
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Close)
  
        # adding action when form is accepted
        # self.buttonBox.accepted.connect(self.getInfo)
  
        # addding action when form is rejected
        self.buttonBox.rejected.connect(self.reject)

        # creating progress bar
        self.pbar = QProgressBar(self)

        # creating push button
        self.btn = QPushButton('Start', self)
  
        # changing its position
        self.btn.move(200, 500)
  
        # adding action to push button
        self.btn.clicked.connect(self.doAction)

        self.folder_picker.clicked.connect(self.selectTarget)
        # calling the method that create the form
        self.createForm()
  
        # creating a vertical layout
        mainLayout = QVBoxLayout()
  
        # adding form group box to the layout
        mainLayout.addWidget(self.formGroupBox)
  
        # adding button box to the layout
        mainLayout.addWidget(self.buttonBox)
  
        # setting lay out
        self.setLayout(mainLayout)
  
    # get info method called when form is accepted
    # def getInfo(self):

    #     # printing the form information
    #     print("Link : {0}".format(self.link.text()))
    #     print("Page : {0}".format(self.pages.currentText()))
    #     print("Output : {0}".format(self.output.text()))
    #     # closing the window
    #     self.close()
    def selectTarget(self):
        dialog = QFileDialog(self)

        if self.output.text():
            dialog.setDirectory(self.output.text())

        dialog.setFileMode(dialog.Directory)

        # we cannot use the native dialog, because we need control over the UI
        options = dialog.Options(dialog.DontUseNativeDialog | dialog.ShowDirsOnly)
        dialog.setOptions(options)

        def checkLineEdit(path):
            if not path:
                return
            if path.endswith(QDir.separator()):
                return checkLineEdit(path.rstrip(QDir.separator()))
            path = QFileInfo(path)
            if path.exists() or QFileInfo(path.absolutePath()).exists():
                button.setEnabled(True)
                return True

        # get the "Open" button in the dialog
        button = dialog.findChild(QDialogButtonBox).button(
            QDialogButtonBox.Open)

        # get the line edit used for the path
        lineEdit = dialog.findChild(QLineEdit)
        lineEdit.textChanged.connect(checkLineEdit)

        # override the existing accept() method, otherwise selectedFiles() will 
        # complain about selecting a non existing path
        def accept():
            if checkLineEdit(lineEdit.text()):
                # if the path is acceptable, call the base accept() implementation
                QDialog.accept(dialog)
        dialog.accept = accept

        if dialog.exec_() and dialog.selectedFiles():
            path = QFileInfo(dialog.selectedFiles()[0]).absoluteFilePath()
            self.output.setText(path)

    def doAction(self):
        for i in range(50):
            # slowing down the loop
            time.sleep(0.03)
  
            # setting value to progress bar
            self.pbar.setValue(i)

        link = self.link.text()
        page = self.pages.currentText()
        output = self.output.text()
        subprocess.call('scrapy crawl main_spider -a link={} -a page={} -a output={}'.format(link, page, output), shell=True)

        for i in range(50, 101):
            # slowing down the loop
            time.sleep(0.03)
  
            # setting value to progress bar
            self.pbar.setValue(i)

    # creat form method
    def createForm(self):
  
        # creating a form layout
        layout = QFormLayout()
  
        # adding rows
        # for name and adding input text
        layout.addRow(QLabel("Link"), self.link)
  
        # for degree and adding combo box
        layout.addRow(QLabel("Pages"), self.pages)
  
        # for age and adding spin box
        layout.addWidget(self.output)
        layout.addWidget(self.folder_picker)

        layout.addRow(QLabel('Progress'), self.pbar)

        layout.addRow(QLabel("Start crawl"), self.btn)
  
        # setting layout
        self.formGroupBox.setLayout(layout)
  
  
# main method
if __name__ == '__main__':
  
    # create pyqt5 app
    app = QApplication(sys.argv)
  
    # create the instance of our Window
    window = Window()
  
    # showing the window
    window.show()
  
    # start the app
    sys.exit(app.exec())